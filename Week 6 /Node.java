    public Node(String key, String value) {
        this.key = key;
        this.value = value;
        this.next = null;
    }

    public String getValue() {
        return value;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Node(String key) {
        this.key = key;
    }

    public Node getNext() {
        return next;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
