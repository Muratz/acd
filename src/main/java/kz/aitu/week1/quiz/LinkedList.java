package kz.aitu.week1.quiz;


import javafx.scene.Node;

public class LinkedList{
    private final Node head;
     private Node tail;

    public LinkedList(){
        this.head = new Node("head");
        tail = head;
    }

    public Node head(){
        Node head = this.head;
        return head;
    }

    public void next(Node node){
        tail.next = node;
        tail = node;
    }
}
