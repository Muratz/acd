package kz.aitu.week1;

public class BigO {

    //O(1)
    public static void func1(int n) {
        System.out.println("Hey - your input is: " + n);

        System.out.println("Hey - your input is: " + n);
        System.out.println("Hmm.. I'm doing more stuff with: " + n);
        System.out.println("And more: " + n);
    }

    //O(n)
    public static void func2(int n) {
        for (int i = 0; i < n; i++) {
            System.out.println("Hey - I'm busy looking at: " + i);
            System.out.println("Hmm.. Let's have another look at: " + i);
            System.out.println("And another: " + i);
        }
    }

    //O(log(n))
    public static void func3(int n) {
        for (int i = 1; i < n; i = i * 2){
            System.out.println("Hey - I'm busy looking at: " + i);
        }
    }

    //O(nlog(n))
    public static void func4(int n) {
        for (int i = 1; i <= n; i++){
            for(int j = 1; j < n; j = j * 2) {
                System.out.println("Hey - I'm busy looking at: " + i + " and " + j);
            }
        }
    }

    //O(1)
    public static void func5(int n) {

        for (int i = 1; i <= 8; i++) {
            System.out.println("Hey - I'm busy looking at: " + i);
        }
    }

    //O(n^2)
    public static void func6(int n) {
        for (int i = 1; i <= n; i++) {
            for(int j = 1; j <= n; j++) {
                System.out.println("Hey - I'm busy looking at: " + i + " and " + j);
            }
        }
    }

    //(O(2^n))
    public static void func7(int n) {
        for (int i = 1; i <= Math.pow(2, n); i++){
            System.out.println("Hey - I'm busy looking at: " + i);
        }
    }

    public static void main(String[] args) {
        int n = 2;
        func5(n);
    }
}
