package kz.aitu.week1;

import java.util.Scanner;

public class Practice7 {

    public int findSecondMax(int max1, int max2) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();

        if(a == 0) return max2;
        if(a > max1) {
            max2 = max1;
            max1 = a;
        }
        else if(a > max2) max2 = a;

        return findSecondMax(max1, max2);
    }

    public void print(int n) {
        if(n==0) return;
        System.out.print(n + " ");
        print(n-1);
    }

    public void fun(int a){
        if (a == 0) return;
        Scanner scanner1 = new Scanner(System.in);
        int b = scanner1.nextInt();
        fun(a - 1);
        System.out.println(b);
    }



    public void run() {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        fun(a);
    }
}
