public class Main {
    public static void main(String[] args) {
        Queue queue = new Queue();
        queue.add("Ferrari");
        queue.add("Chevrolet");
        queue.add("Honda");
        queue.add("Hyundai");
        queue.add("Nissan");

        System.out.println(queue.size());
        System.out.println(queue.poll());
        System.out.println(queue.peek());
        System.out.println(queue.poll());
        System.out.println(queue.poll());
        System.out.println(queue.size());
        System.out.println(queue.empty());
        }
    }
}