public class Main {
    public static int Acquire(int arr[], int n) {
        int toAcquire = arr[0];

        for (int i = 1; i < n; i++)
            toAcquire = Math.min(toAcquire, arr[i]);
        return toAcquire;
    }

    public static int Sell(int arr[], int n) {
        int sell = arr[0];

        for (int i = 1; i < n; i++)
            sell = Math.max(sell, arr[i]);
        return sell;
    }

    public static void main(String[] args) {
        int arr[] = { 10, 7, 5, 8, 11 };
        int n = arr.length;
        System.out.println( "Acquire - "+ Acquire(arr, n));
        System.out.println( "Sell - " + Sell(arr, n));
    }

}
