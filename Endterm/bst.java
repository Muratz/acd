public class bst {

    private static Node root;

    public bst() {
        this.root = null;
    }

    public void insert(Integer key, String value) {
        Node node = new Node(key, value);
        if (root == null)
            root = node;
        else {
            Node current = root;
            while (current != null) {
                if (key < current.getKey()) {
                    if (current.getLeft() == null) {
                        current.setLeft(node);
                        return;
                    }
                    current = current.getLeft();
                } else if (key > current.getKey()) {
                    if (current.getRight() == null) {
                        current.setRight(node);
                        return;
                    }
                    current = current.getRight();
                }
            }
        }
    }


    public String find(Integer key) {
        Node current = root;
        while (current != null) {
            if (key > current.getKey()) current = current.getRight();
            else if (key < current.getKey()) current = current.getLeft();
            else return current.getValue();

        }
        return null;
    }

    public boolean delete() {

        return true;

    }


    private Node findNode(Node node, Integer key) {
        if (node == null) return null;
        if (key > node.getKey()) return findNode(node.getRight(), key);
        else if (key < node.getKey()) return findNode(node.getLeft(), key);
        else return node;
    }


    public void printAll() {
        Node node = root;
        while (node != null) {
            if (node.getLeft() != null) {
                node = node.getLeft();
                return;

            }
            System.out.println(node.getValue());
            if (node.getRight() != null) {
                node = node.getRight();

                }
                return;
            }
            return;
        }
    }

